<?xml version="1.0" encoding="utf-8"?>
<BlogCollection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <BlogPosts>
    <BlogPost>
      <Id>543659c9-b8b7-4264-a6f1-ab17dc945dcd</Id>
      <Author />
      <Content>&lt;p&gt;In 2015, Efficiency Vermont supported a research project conducted by Maclay Architects to examine the financial feasibility of net zero energy buildings in Vermont and beyond. The study provides background for developers, contractors, designers, consultants, and building clients that will show the relevance and financial benefit of building beyond code standards to net zero energy standards.
&lt;/p&gt;
&lt;p&gt; &lt;!-- download for template &lt;a target="_blank" class="button small" href="/docs/about_efficiency_vermont/whitepapers/NZFsummary.pdf"&gt;Download the Summary Report (pdf)&lt;span class = "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p&gt; &lt;a target="_blank" class="button small" href="/docs/about_efficiency_vermont/whitepapers/NZFfullReport_web.pdf"&gt;Download the Final Report (pdf)&lt;span class = "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p&gt; &lt;a target="_blank" class="button small" href="/docs/about_efficiency_vermont/whitepapers/net-zero-webinar-slides.pdf"&gt;Download the Webinar Slides (pdf)&lt;span class = "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt; --&gt;
&lt;!-- video for template maybe? &lt;p&gt; &lt;a target="_blank" class="button small" href=" https://www.youtube.com/watch?v=viqvhSJWo38"&gt;Watch the Webinar&lt;/a&gt;&lt;/p&gt; --&gt;</Content>
      <PublicationDate>2015-03-12T18:23:50.483</PublicationDate>
      <Thumbnail>/images/about_us/whitepapers/evt-commercial-netzero-thumbnail.jpg</Thumbnail>
      <Title>Net Zero Energy Feasibility Study</Title>
      <UrlName>net-zero-energy-feasibility-study</UrlName>
      <BlogComments />
      <Categories>Energy Issues in Vermont</Categories>
    </BlogPost>
    <BlogPost>
      <Id>ebb47f37-272c-61a3-a6b6-ff0000e632b6</Id>
      <Content>&lt;p&gt;In 2013, Efficiency Vermont conducted a field research study to examine the potential of identifying the “heat-loss rate” of buildings with thermostat data. Researchers explored the hypothesis that a programmable communicating thermostat paired with location, weather, and other broadly available information sources could be used to discern meaningful differences in household thermal performance. A small sample of data from 13 thermostats in Vermont homes and small businesses was used to complete the study. Results suggest that using remote diagnostics incorporating data from thermostats may be a cost-effective strategy for efficiency programs and could provide meaningful site-specific insights.   
&lt;/p&gt; &lt;!-- download for template &lt;p&gt; &lt;a target="_blank"  href="/docs/about_efficiency_vermont/whitepapers/Nest_WhitePaper.pdf"&gt;Download the Final Report (pdf)&lt;span class = "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt; --&gt;</Content>
      <PublicationDate>2014-06-25T13:39:03.13</PublicationDate>
      <Thumbnail>/images/about_us/whitepapers/thermal_thumbnail.jpg</Thumbnail>
      <Title>Are Thermostats the New Energy Audits?</Title>
      <UrlName>are-thermostats-the-new-energy-audits</UrlName>
      <BlogComments />
      <Categories>Technology</Categories>
    </BlogPost>
    <BlogPost>
      <Id>d3c87e37-272c-61a3-a6b6-ff0000e632b6</Id>
      <Content>&lt;p&gt;The Efficiency Vermont Home Performance with ENERGY STAR program officially launched in 2005. In 2008, when the Vermont Legislature set a statewide goal to weatherize 80,000 homes by the year 2020, the program was galvanized. The legislation sought to harness energy efficiency as a driver of savings for consumers and economic development in Vermont. In order to meet the goal, Vermont’s residential retrofit programs would need to scale up.&lt;/p&gt; &lt;p&gt;This report was prepared to provide an overview and analysis of results and lessons learned under Efficiency Vermont’s program from 2011-2013. The aim was to explore the following questions:&lt;/p&gt; &lt;ul&gt; &lt;li&gt;What are the factors that motivate customers to initiate and complete retrofit projects?&lt;/li&gt; &lt;li&gt;How have these factors influenced the growth and development of the Home Performance with ENERGY STAR program through 2013?&lt;/li&gt; &lt;li&gt;What are the best options for cost-effectively increasing project completions while maintaining a market-based program structure?&lt;/li&gt; &lt;li&gt;What other opportunities exist to update and evolve the program, setting the stage to accommodate and encourage further growth?&lt;/li&gt; &lt;/ul&gt; &lt;figure&gt; &lt;img src="/Images/blog/2014-06-16/evt-home-performance-energy-star-web.jpg" alt="Home Performance with Energy Star project completions 2005-13" /&gt; &lt;figcaption&gt;Project completions have increased steadily over time&amp;mdash;nearly quadrupling between
2008-2013.&lt;/figcaption&gt; &lt;/figure&gt; &lt;div class="key-recommendations" &gt; &lt;h3&gt;Key Findings&lt;/h3&gt; &lt;ul&gt; &lt;li&gt;There was a 377% increase in the number of homes retrofitted through Efficiency Vermont's program from 2008-2013&lt;/li&gt; &lt;li&gt;There is a strong correlation between statewide marketing and customer interest in thermal efficiency&lt;/li&gt; &lt;li&gt;Incentives are the single most important factor in converting energy audits into project completions&lt;/li&gt; &lt;/ul&gt; &lt;/div&gt; &lt;br&gt; &lt;p&gt;These are questions that are frequently posed to the staff of Efficiency Vermont by contractors, partner organizations, and others who are engaged in the broader effort to help Vermont meet the 80,000 homes retrofit goal by 2020. Several other studies in recent years have also sought to address these questions. The Public Service Department provides robust third-party evaluation and oversight of all Efficiency Vermont programs, and in 2013 completed both an impact and process evaluation of the Efficiency Vermont Home Performance with ENERGY STAR program. The High Meadows Fund also helped to conduct market research and convened focus groups exploring the barriers and potential motivations behind the decision-making of single-family homeowners to complete retrofits. These studies have all provided valuable insights that Efficiency Vermont is currently working to incorporate into its program design and delivery.&lt;/p&gt; &lt;p&gt;This report was developed to complement that work by providing additional perspective from Efficiency Vermont, based on a detailed analysis of program data. It is not a substitute for independent evaluation.&lt;/p&gt; &lt;p&gt;It is our hope that the data and analysis included in this report will serve as a resource to inform the conversations that are taking place around the 80,000 homes retrofit goal, deepen engagement and collaboration among partners, and provide transparency about the operations and long term strategy of the Efficiency Vermont Home Performance with ENERGY STAR program.&lt;/p&gt; &lt;!-- download for template &lt;p&gt; &lt;a target="_blank" href="/docs/about_efficiency_vermont/whitepapers/evt-home-performance-energy-star-report.pdf"&gt;Download the Final Report (pdf)&lt;span class = "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt; --&gt;</Content>
      <PublicationDate>2014-06-16T13:44:15.753</PublicationDate>
      <Thumbnail>/images/about_us/whitepapers/evt-home-performance-energy-star.jpg</Thumbnail>
      <Title>Efficiency Vermont's Home Performance with ENERGY STAR&lt;sup&gt;&amp;reg;&lt;/sup&gt; Program Report and Analysis</Title>
      <UrlName>efficiency-vermont's-home-performance-with-energy-star-program-report-and-analysis</UrlName>
      <BlogComments />
      <Categories>Challenges and Solutions</Categories>
    </BlogPost>
    <BlogPost>
      <Id>4f8b7c37-272c-61a3-a6b6-ff0000e632b6</Id>
      <Content>&lt;p&gt;
In 2013, Efficiency Vermont supported a small research and development project to examine the potential impact of electric vehicles on Vermont's power infrastructure. The study outlines various ways in which electric vehicles can serve as resources to the electric grid, in the context of Vermont's Comprehensive Energy Plan goal to power 25% of vehicles with renewable energy sources by 2030.
&lt;/p&gt; &lt;!-- download for template  &lt;p&gt; &lt;a target="_blank"  href="/docs/about_efficiency_vermont/whitepapers/evt-rd-electric-vehicles-grid-resource-final-report.pdf"&gt;Download the Final Report (pdf)&lt;span class = "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt; --&gt;</Content>
      <PublicationDate>2014-05-12T13:37:38.147</PublicationDate>
      <Thumbnail>/images/about_us/whitepapers/evt-electric-vehicles-grid-resource-thumb.jpg</Thumbnail>
      <Title>Electric Vehicles as Grid Resources in ISO-NE and Vermont</Title>
      <UrlName>electric-vehicles-as-grid-resources-in-iso-ne-and-vermont</UrlName>
      <BlogComments />
      <Categories>Technology</Categories>
    </BlogPost>
    <BlogPost>
      <Id>58e87937-272c-61a3-a6b6-ff0000e632b6</Id>
      <Content>&lt;p&gt;In 2008, the Vermont legislature set a goal to improve the energy efficiency of 25% of the state&amp;rsquo;s homes by the
year 2020. In 2013 a &amp;ldquo;Thermal Efficiency Task Force,&amp;rdquo; convened by the Vermont Department of Public Service,
reported that the state was on track to meet only half of that goal. At the same time, Vermont has seen a surge
of community-based organizing and activity on sustainable energy over the last decade. As of 2013, the Vermont
Energy and Climate Action Network (VECAN),
which serves as an umbrella network for the state&amp;rsquo;s
town energy committees, counted more than
150 towns with a local energy committee or local
energy coordinator. Such groups have organized
effectively to promote a range of activities
including retrofitting schools and municipal
buildings, siting small-scale renewable energy
projects, and engaging their neighbors and town
leaders in energy planning efforts.&lt;/p&gt; &lt;p&gt;The Vermont Home Energy Challenge (VHEC)
was a yearlong engagement effort to test the
potential of local community organizations to help
raise awareness of energy efficiency and increase
completion of projects under Efficiency Vermont&amp;rsquo;s
Home Performance with ENERGY STAR &amp;reg; program.
VHEC was designed and launched through a
partnership of Efficiency Vermont and &lt;abbr title = "Vermont Energy and Climate Action Network"&gt;VECAN&lt;/abbr&gt;,
and it encompassed a range of activities over the
course of 2013, with a special focus on: Turnkey outreach efforts; the distribution of pledge cards committing
signers to take action on home energy efficiency; mini grants for local energy groups; training and support of
local volunteers; and marketing materials designed to raise the visibility of local energy efforts.&lt;/p&gt; &lt;!-- download for template &lt;p&gt;&lt;a href="~/docs/about_efficiency_vermont/whitepapers/VHEC_FINAL_Report_3_27_2014.pdf" target="_blank"&gt;Download and Read the Full White Paper (pdf)&lt;span class = "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt; --&gt;</Content>
      <PublicationDate>2014-03-28T13:25:58.977</PublicationDate>
      <Thumbnail>/Images/about_us/whitepapers/hec_logo.png</Thumbnail>
      <Title>The Vermont Home Energy Challenge Final Report</Title>
      <UrlName>the-vermont-home-energy-challenge-final-report</UrlName>
      <BlogComments />
    </BlogPost>
    <BlogPost>
      <Id>f6267937-272c-61a3-a6b6-ff0000e632b6</Id>
      <Content>&lt;p&gt;The Vermont legislature has established an ambitious goal to improve the energy fitness of 25% of Vermont&amp;rsquo;s housing units, about 80,000 homes and apartments, by 2020. This will require a substantial increase in the number of Vermont households completing energy efficiency improvements. In 2012 and 2013, the &lt;a href="http://www.highmeadowsfund.org/" target = "_blank"&gt;High Meadows Fund&lt;/a&gt;, in collaboration with Efficiency Vermont, commissioned market research and focus groups to explore the barriers and potential motivations behind the decision-making of single-family homeowners. &lt;/p&gt; &lt;p&gt;The market research included a survey of 615 Vermont homeowners not known to have participated in either the &lt;a href="http://www.efficiencyvermont.com/For-My-Home/ways-to-save-and-rebates/Audits-Heating-Insulation/Overview"&gt;Efficiency Vermont Home Performance with ENERGY STAR&lt;/a&gt; program or the &lt;a href="http://www.vermontgas.com/efficiency_programs/res_programs.html#retrofit" target = "_blank"&gt;Vermont Gas Home Retrofit&lt;/a&gt; program. The survey collected information on recent home energy upgrades; barriers to and motivations for pursuing energy efficiency improvements; awareness of retrofit programs; and interest in potential program services. Building on this research, focus groups examined Vermont homeowners&amp;rsquo; interest in and experience with energy efficiency improvements in greater depth, with a focus on potential measures to increase their confidence that the improvements would reduce energy bills.  &lt;/p&gt; &lt;!-- download for template &lt;p&gt; &lt;a target="_blank"  href="/docs/about_efficiency_vermont/whitepapers/market-research-report-2013-02-15.pdf"&gt;Download Part 1 of the Research Report (pdf)&lt;span class = "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt; &lt;p&gt; &lt;a target="_blank"  href="/docs/about_efficiency_vermont/whitepapers/focus-groups-report-Dec-2014.pdf"&gt;Download Part 2 of the Research Report (pdf)&lt;span class = "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt; --&gt;</Content>
      <PublicationDate>2014-02-05T20:48:09.697</PublicationDate>
      <Thumbnail>/images/about_us/whitepapers/SFRetro-Research-Thumb.jpg</Thumbnail>
      <Title>Market Research on Barriers and Motivations for Home Energy Efficiency Improvements</Title>
      <UrlName>market-research-on-barriers-and-motivations-for-home-energy-efficiency-improvements</UrlName>
      <BlogComments />
    </BlogPost>
    <BlogPost>
      <Id>66657637-272c-61a3-a6b6-ff0000e632b6</Id>
      <Content>&lt;p&gt;For most people, compact fluorescent light bulbs (or CFLs) are the face of energy efficiency. This is not surprising, given that the CFL has utterly transformed energy efficiency over the last decade. The rise of the CFL has enabled massive savings for energy efficiency programs, and has greatly expanded the reach of energy efficiency efforts by serving as a low cost, first point of entry for consumers looking to reduce their electric bills.&lt;/p&gt; &lt;p&gt;If we want to continue realizing savings in the residential market, the reign of the CFL is by&amp;nbsp;no means over. Although there are a range of&amp;nbsp;residential efficient lighting products that will&amp;nbsp;present further opportunities for savings, the CFL&amp;nbsp;still has great potential because of its relatively&amp;nbsp;low cost and accessibility to consumers. As we&amp;nbsp;transition to other efficient products in the future,&amp;nbsp;it has also provided energy efficiency programs&amp;nbsp;with a valuable framework for understanding how&amp;nbsp;to better engage consumers with their energy use.&lt;/p&gt; &lt;div class="key-recommendations"&gt; &lt;h3&gt;Key Insights&lt;/h3&gt; &lt;ul&gt; &lt;li&gt;CFL bulbs will continue to be the most widely&lt;br /&gt;
    accessible and affordable energy efficiency&amp;nbsp;opportunity for homeowners&lt;/li&gt; &lt;li&gt;The potential for home energy savings from lighting&amp;nbsp;is still quite large: In Vermont, energy efficient bulbs&lt;br /&gt;
    are present in just 33% of light sockets&lt;/li&gt; &lt;li&gt;As the lighting market continues its rapid evolution,oversight and testing by regulatory authorities (such&amp;nbsp;as ENERGY STAR &amp;reg;) will play a critical role in guiding&amp;nbsp;consumer choices&lt;/li&gt; &lt;/ul&gt; &lt;/div&gt; &lt;h3&gt;The Rise of the CFL&lt;/h3&gt; &lt;div &gt;&lt;figure&gt;&lt;img alt="" src="/Images/about_us/whitepapers/CFL_WhitePaper.png" /&gt;&lt;figcaption&gt; Caption here if needed &lt;/figcaption&gt;&lt;/figure&gt; &lt;/div&gt;
The CFL is the iconic symbol of energy efficiency. It stands for the simplicity of the movement: the investment,&lt;br /&gt;
the innovation, and the reward. But the leap of faith from incandescent to CFL meant abandoning over 100&lt;br /&gt;
years of reliance on a single emblem of light and an established home experience. The persistence of the&lt;br /&gt;
incandescent can largely be attributed to its reliability and ubiquity, and the impact it had on the direction of&lt;br /&gt;
our human experience. Over the last century, incandescent light bulbs influenced not only our relationship with&lt;br /&gt; &lt;p&gt;our homes, but also the design and development of our communities, and the way that we structure our lives.&lt;/p&gt; &lt;p&gt;So why change? Ninety percent of the power consumed by an incandescent is emitted as heat. That means that only 10% of power consumption ends up as light we see. We accepted this fact for over a century. As a new paradigm of limited resources has begun to emerge, humans have recognized the need to be more thoughtful in our energy consumption and develop new technologies such as the CFL.&amp;nbsp;&lt;/p&gt; &lt;p&gt;The CFL had a rocky start. The new technology was sent out into the market by many different manufacturers with little consistency. Characteristics between products were very different as well, especially in comparison with the incandescent bulbs that they were supposed to replace. By the time regulatory agencies and quality testing bodies began their work to ensure the overall quality of CFLs, the public was already having mixed experiences. The result is that even now, when the industry as a whole has learned from its mistakes, it has taken a good deal of effort to regain the trust of consumers. However, CFLs have great potential to save energy and money; a CFL uses one-fifth to one-third of the amount of energy that an incandescent uses and lasts almost five times as long.&lt;/p&gt; &lt;!-- download for template &lt;p&gt;&lt;a target="_blank" href="~/docs/about_efficiency_vermont/whitepapers/White_Paper_Bonn.pdf"&gt;Download and Read the Full White Paper (pdf)&lt;span class = "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt; --&gt; &lt;p&gt;Lara Bonn,&amp;nbsp;Retail Efficient Products Program Manager&lt;/p&gt;</Content>
      <PublicationDate>2013-09-20T20:03:18</PublicationDate>
      <Thumbnail>/Images/about_us/whitepapers/CFL_WhitePaper.png</Thumbnail>
      <Title>The Once and Future CFL</Title>
      <UrlName>The_Once_and_Future_CFL</UrlName>
      <BlogComments />
      <Categories>Trends</Categories>
    </BlogPost>
    <BlogPost>
      <Id>6e657637-272c-61a3-a6b6-ff0000e632b6</Id>
      <Content>&lt;p&gt;Energy efficiency service providers, such as Efficiency Vermont, know that an understanding of the customer is essential to
their effectiveness. Multiple factors&amp;mdash;including customers&amp;rsquo; priorities, budgets, depth of efficiency knowledge and more&amp;mdash;have
an impact on customers&amp;rsquo; decisions to participate in their programs. Mature programs seek to engage customers through
multiple channels that generally include a website, rebates and incentives, marketing, and key account management to name
just a few. As we strive to move beyond these basic offerings, a
deeper understanding of what it means to truly engage customers
can provide valuable insights into every facet of energy efficiency
program design. The ultimate goal is to engage customers on an
optimum level in ways that will enable them to reduce their energy
usage, and motivate them to continue seeking even greater savings.&lt;/p&gt; &lt;div class="key-recommendations"&gt; &lt;h3&gt;Key Insights&lt;/h3&gt; &lt;ul&gt; &lt;li&gt;Recognize and value the individual
    journey of participation; build customer
    knowledge and capability through
    multiple interactions&lt;/li&gt; &lt;li&gt;Create Initiatives that are simple and
    accessibly engaging, but not effortless&lt;/li&gt; &lt;li&gt;Make the path(s) forward clear,
    progressively challenging, and rewarding&lt;/li&gt; &lt;li&gt;Build customer feelings of investment in
    successful outcomes and results&lt;/li&gt; &lt;/ul&gt; &lt;/div&gt; &lt;h3&gt;Give the People What They Want: When Customers are Eager Participants&lt;/h3&gt; &lt;p&gt;The success of an efficiency initiative is built upon a foundation of
effective customer interactions to deliver cost-effective energy savings
and meet broad goals [Figure 1].&lt;/p&gt; &lt;p&gt;Customers are a diverse group that includes every ratepayer in the service territory, from large industrial and commercial facilities to
institutions, small businesses, multifamily residences, homeowners, and everyone in between. This incredible diversity of audience has
always posed a significant challenge for the design and effectiveness of efficiency program outreach.&lt;/p&gt; &lt;div &gt;&lt;figure&gt; &lt;img width="400px" height="400px" alt="efficiency feedback loop" src="/Images/about_us/whitepapers/figure1.png" /&gt;&lt;figcaption&gt;Figure 1: efficiency utility savings come from supporting customer energy actions &lt;/figcaption&gt;&lt;/figure&gt; &lt;/div&gt; &lt;p&gt;How do you reach the right customers with the most effective messages&amp;mdash;and motivate them to take action? As it turns
out, motivating customers requires more than knowing who they are and what value you have to offer them: It is about
understanding their experience&amp;mdash;and turning
what they need into what they want&amp;mdash;and
even enjoy.&lt;/p&gt; &lt;p&gt;The psychology of engagement has been
studied and increasingly understood by
scientists, economists, and other experts
for years. Conventional energy efficiency
programs, in contrast, haven&amp;rsquo;t yet broadly
integrated these perspectives and lessons,
and are likely missing tremendous
opportunities for greater savings. The real
loss, however, is that efficiency programs
are not enabling people to become true
participants, rather than just customers.&lt;/p&gt; &lt;!-- download for template &lt;p&gt;&lt;a href="/docs/about_efficiency_vermont/whitepapers/White_Paper_Lange_August2013.pdf" target="_blank" class="button small"&gt;Download and Read the Full White Paper (pdf)&lt;span class = "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;--&gt; &lt;p &gt;Nick Lange, Energy Efficiency Consultant&lt;/p&gt;</Content>
      <PublicationDate>2013-08-14T18:00:00</PublicationDate>
      <Thumbnail>/images/about_us/whitepapers/figure-1-web.jpg</Thumbnail>
      <Title>Individual and Community Engagement</Title>
      <UrlName>Individual_and_Community_Engagement</UrlName>
      <BlogComments />
      <Categories>Trends</Categories>
    </BlogPost>
    <BlogPost>
      <Id>76657637-272c-61a3-a6b6-ff0000e632b6</Id>
      <Content>&lt;p&gt;Lighting has played a central role in energy efficiency programs for decades. Often dubbed low-hanging fruit, lighting
has presented savings opportunities for nearly every customer and has offered efficiency programs the ability to obtain
a magnitude of energy savings unmatched by any other single technology. In Vermont, after years of successful efficient
fluorescent lighting promotion, the capture of available lighting savings may seem nearly complete. This high level of
fluorescent adoption, however, comes at a time of tremendous advancement for light emitting diode (LED) technology.
By some estimates, as much as half of Vermont&amp;rsquo;s lighting savings on the commercial side are yet to be realized, due to
the savings potential of LEDs, when combined with the use of lighting controls and design. The obstacles that once stood
in the way of widespread LED adoption are rapidly disappearing, and we can look forward to a new era of unprecedented
lighting savings.&lt;/p&gt; &lt;div class="key-recommendations"&gt; &lt;h3&gt;Key Insights&lt;/h3&gt; &lt;ul&gt; &lt;li&gt;LED technology is advancing rapidly, and it is already
    more cost-effective than fluorescent lighting for
    some applications&lt;/li&gt; &lt;li&gt;As much as half of Vermont&amp;rsquo;s potential energy
    savings from commercial lighting upgrades have not
    yet been realized&lt;/li&gt; &lt;li&gt;LED technology already accounts for a majority
    of lighting savings in Vermont&amp;rsquo;s commercial and
    industrial sector&amp;ndash;by 2018, the share of savings due
    to LEDs will be more than 80%&lt;/li&gt; &lt;li&gt;Lighting design and controls can maximize the
    energy saving potential and overall lighting
    performance of LEDs&lt;/li&gt; &lt;/ul&gt; &lt;/div&gt; &lt;h3&gt;The Flourescent Era&lt;/h3&gt; &lt;p&gt;When Efficiency Vermont was formed, in 1999,
T8 fluorescent fixtures and compact fluorescent
lamps (CFLs) were heavily promoted to customers
as efficient alternatives to older T12 fluorescent
and/or incandescent lighting. The reasons were
obvious: Most customers had opportunities for
lighting upgrades; the resulting energy savings
were cost effective; and&amp;ndash;in most cases&amp;ndash;the new
technologies offered significant improvements in
light quality. Due to incremental improvements to
fluorescent technology over the years, today&amp;rsquo;s high
performance T8 (HPT8) and CFL products offer
superior performance and energy efficiency.&lt;/p&gt; &lt;p&gt;&lt;figure&gt;&lt;img src="/Images/about_us/whitepapers/white-paper-graph-7-09-web4.jpg" alt="industrial lighting savings graph" /&gt;&lt;figcaption&gt; Caption here if needed &lt;/figcaption&gt;lt;/figure&gt;&lt;/p&gt; &lt;p&gt;Despite these improvements, fluorescent technology has a limited future in Vermont as a source of new energy demand
reductions. The technology is already near its energy efficiency peak and most customers have already upgraded to some
form of efficient fluorescent lighting. Within Vermont, 83% of all commercial fluorescent equipment is now an efficient
technology (T8, HPT8, or T5) and 92% of homes statewide use some CFLs.&lt;/p&gt; &lt;h3&gt;Enter LEDs&lt;/h3&gt; &lt;p&gt;Historically, lighting has accounted for approximately 70% of energy saved through Efficiency Vermont&amp;rsquo;s programs. As
recently as 2009, LEDs represented a mere 1% of that total. In just the last three years, however, the LED contribution to
overall Efficiency Vermont savings grew dramatically to nearly 25%. Within the commercial and industrial sector, LEDs
now account for 55% of savings across all lighting technologies.&lt;/p&gt; &lt;p&gt;There are many reasons for the current trend toward rapid
adoption of LEDs. First, they can match the light output,
distribution, and color that customers demand while offering
incredibly long lifetimes. Additionally, LEDs do not present
many of the challenges that dogged earlier efficient lighting
technology, such as mercury content, flickering, and limited
dimming range. Most notably, LEDs deliver up to 80% energy
savings, depending on the technology that is replaced.&lt;/p&gt; &lt;p&gt;To date, Efficiency Vermont has promoted LEDs predominately
for commercial exterior and select directional interior uses. In
such commercial settings, where operating hours for lighting
are much longer than in homes, the advantages of LEDs make them financially feasible, despite their higher upfront
cost. Additionally, LEDs are better suited than fluorescents for exterior and many interior settings because they can be
directionally controlled (to create a spotlight) and perform well in cold temperatures. These applications offer a significant
opportunity for energy savings; however, the much larger opportunity of commercial interior general lighting remains
mostly untouched.&lt;/p&gt; &lt;!-- download for template &lt;p&gt;&lt;a href="/docs/about_efficiency_vermont/whitepapers/White_Paper_Mellinger.pdf" target="_blank"&gt;Download and Read the Full White Paper (pdf)&lt;span class "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt; --&gt; &lt;p &gt;Dan Mellinger, Lighting Strategy Manager&lt;/p&gt;</Content>
      <PublicationDate>2013-07-10T16:00:22</PublicationDate>
      <Thumbnail>/images/about_us/whitepapers/white-paper-graph-7-09-thumb.jpg</Thumbnail>
      <Title>A New Dawn in Efficient Lighting</Title>
      <UrlName>A_New_Dawn_in_Efficient_Lighting</UrlName>
      <BlogComments />
      <Categories>Challenges and Solutions</Categories>
    </BlogPost>
    <BlogPost>
      <Id>7e657637-272c-61a3-a6b6-ff0000e632b6</Id>
      <Content>&lt;h3&gt;Unleashing Energy Potential&lt;/h3&gt; &lt;p&gt;If you have been paying attention to the energy efficiency field in recent years, you have probably noticed a great deal of excitement about the potential of data. But those of us who work with it every day will tell you that it is not about data; it is about information. Energy data on its own can be illuminating to someone who is trained to analyze it, but it is not useful until it leads to actual energy saving projects. Beyond the immediate benefits of individual efficiency projects, information that can be used to unlock energy savings has a great deal of value in our energy dependent economy. Having concrete information about projected energy savings builds the case that efficiency investments will yield real and lasting value. Ultimately, the goal is to reach a &amp;ldquo;tipping point&amp;rdquo; when all actors in the market have confidence in the accuracy of projected energy savings. This will unlock the power of capital markets to support all cost effective investments in energy efficiency in much the same way that they support the development of other energy sources.&lt;/p&gt; &lt;div class="key-recommendations"&gt; &lt;h3&gt;Key Recommendations&lt;/h3&gt; &lt;ul&gt; &lt;li&gt;Data in context yields valuable energy information&lt;/li&gt; &lt;li&gt;Energy information can uncover new efficiency opportunities, and increase confidence in savings projections&lt;/li&gt; &lt;li&gt;Energy information may ultimately enable efficiency investments to be valued on par with energy generation&lt;/li&gt; &lt;/ul&gt; &lt;/div&gt; &lt;h3&gt;What is energy data &amp;ndash; and why should we care about it?&lt;/h3&gt; &lt;p&gt;Data, by itself, is a raw material, holding potential but not necessarily providing any value. In 2012, most of the electric utility meters in Vermont were converted to &amp;ldquo;smart&amp;rdquo; meters capable of recording hourly or 15 minute interval usage data. These meters have now begun to generate approximately 12 billion data points per year. Add to that the numerous monitoring points from building energy management systems, submeters, and the emergence of smart appliances, and we can expect a dramatic increase in the amount of energy data in the coming years. While this data undoubtedly holds some valuable insights, a simple representation of it provides barely a hint of its underlying meaning (see Figure 1).&lt;/p&gt; &lt;p&gt;&lt;figure&gt;&lt;img src="/Images/about_us/whitepapers/veic-energy-usage.jpg" alt="Energy usage at VEIC" /&gt;&lt;figcaption&gt;Figure 1: Energy usage at VEIC&amp;rsquo;s Vermont offices over 12 months&lt;/figcaption&gt;&lt;/figure&gt;&lt;/p&gt; &lt;p&gt;The use of data producing technologies has largely been driven by the desire to improve the management of existing energy usage and infrastructure. The deployment of Vermont&amp;rsquo;s Advanced Metering Infrastructure (AMI) or &amp;ldquo;smart&amp;rdquo; meters, for example, was primarily undertaken to enable the electric utilities to better understand and manage the grid. Similarly, the installation of electric submeters on individual pieces of building equipment or circuits, as well as networked temperature, light, and occupancy sensors and other building monitoring systems, has been embraced by building owners who want to better understand and manage their buildings&amp;rsquo; energy use. However, the data produced by &lt;abbr title = "Advanced Metering Infrastructure"&gt;AMI&lt;/abbr&gt; and building management systems can also be used to reveal energy saving opportunities. Unlocking the valuable insights encapsulated within energy data requires three critical tools: The data itself; contextual information about the building from which data was gathered (such as the operating schedule, specifications of installed equipment, etc.), and the expertise of an energy data analyst.&lt;/p&gt; &lt;!-- download for template &lt;p&gt; &lt;a href="/docs/about_efficiency_vermont/whitepapers/White-Paper-Big-Savings.pdf"  target="_blank"&gt;Download and Read the Full White Paper (pdf)&lt;span class = "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt; --&gt; &lt;p &gt;Ethan Goldman, Energy Informatics Architect at VEIC&lt;/p&gt;</Content>
      <PublicationDate>2013-05-03T15:09:55</PublicationDate>
      <Thumbnail>/images/about_us/whitepapers/veic-energy-usage-thmb.jpg</Thumbnail>
      <Title>Finding Big Savings in Big Data: Unlocking the full value of energy efficiency</Title>
      <UrlName>Finding_Big_Savings_in_Big_Data_Unlocking_the_full_value_of_energy_efficiency</UrlName>
      <BlogComments />
      <Categories>Challenges and Solutions</Categories>
    </BlogPost>
    <BlogPost>
      <Id>b8657637-272c-61a3-a6b6-ff0000e632b6</Id>
      <Content>&lt;p&gt;At most facilities, everyone consumes energy but only a few are accountable for the costs incurred.  Continuous Energy Improvement (CEI) is a new model in energy efficiency which assures, in effect, that energy efficiency and conservation become an integral part of the business culture, and that controlling energy costs is important to everyone.  The &lt;abbr title = "Continuous Energy Improvement"&gt;CEI&lt;/abbr&gt; model goes far beyond capital upgrades &amp;mdash; which have traditionally been the major driver of business energy efficiency initiatives &amp;mdash; and applies innovative strategies to achieve process improvements, update maintenance cycles, and increase employee engagement. This holistic, long-term, and data-driven approach enables businesses to fully understand how they use energy, and to also generate an actionable "roadmap" for effectively managing this critical component of their production costs against a variable landscape.&lt;/p&gt; &lt;div class="key-recommendations"&gt; &lt;h3&gt;Key Recommendations&lt;/h3&gt; &lt;ul&gt; &lt;li&gt;Analyze energy consumed at every stage
    in the production process&lt;/li&gt; &lt;li&gt;Use data to set long-term energy goals&lt;/li&gt; &lt;li&gt;Create a culture of engagement
    among employees&lt;/li&gt; &lt;li&gt;Continually seek opportunities to achieve
    your energy goals&lt;/li&gt; &lt;/ul&gt; &lt;/div&gt; &lt;h3&gt;Measuring and Evaluating Energy Costs in a Variable Environment&lt;/h3&gt; &lt;p&gt;Many organizations strive for continuous improvement across the full range of their operations, including safety, productivity, and quality.  Similarly, many businesses are taking strong steps to integrate data as a key driver in setting strategic goals and informing tactical decisions.  These efforts are influenced by increasingly complex and rapidly changing business conditions which necessitate that we all access and analyze a profusion of information, every day, to achieve success and thrive.  In other words, no intelligent decision can be made in a vacuum &amp;ndash; and every choice must be subject to regular re-evaluation, with an eye towards constant improvement.&lt;/p&gt; &lt;p&gt;&lt;figure&gt;&lt;img src="/Images/for-my-business/elc/cei-diagram.png" alt="CEI" /&gt;&lt;figcaption&gt;The four components of a Continuous Energy Improvement approach can be applied by any business to take control of energy costs.&lt;/figcaption&gt;&lt;/figure&gt;A common &amp;mdash; and faulty &amp;mdash; assumption is that it is not possible to control expenses related to energy.  This is simply not the case.  Energy, like economics, can be divided into two parts: the supply side and the demand side.  The demand side is where all businesses can make a difference through thoughtful implementation of energy efficiency technologies and by adjusting their approach, philosophy, and processes to promote energy conservation.&lt;/p&gt; &lt;p&gt;That being said, you cannot manage something you are not measuring. With the price of metering equipment having declined over the past few years, most processes can be monitored at a relatively low cost.  A truly effective measurement and management approach requires establishing performance targets, monitoring current status, sharing results with management and staff, and continually taking action across the organization to improve performance over time.  Recently, many tools have begun emerging in the marketplace to empower companies to bring this level of energy management and accountability to tracking, understanding, and controlling energy consumption and costs.  At Efficiency Vermont, we are one of several leading edge utilities and energy efficiency organizations that have worked to support the adoption of these tools among our customers. The results from program participants have been very positive with demonstrated success in implementing and realizing cost-effective savings from Continuous Energy Improvement (CEI).&lt;/p&gt; &lt;!-- download for template &lt;p&gt; &lt;a href="/docs/about_efficiency_vermont/whitepapers/greg-baker-continuous-improvement.pdf" class="button small" target="_blank"&gt; Download and Read the Full White Paper (pdf)&lt;span class = "icon-file-pdf"&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt; --&gt;&lt;p &gt;Greg Baker, Senior Account Manager&lt;/p&gt;</Content>
      <PublicationDate>2013-03-21T13:47:00</PublicationDate>
      <Thumbnail>/images/about_us/whitepapers/cei-diagram-thmb.png</Thumbnail>
      <Title>Continuous Energy Improvement brings efficiency to the next level</Title>
      <UrlName>Continuous_Energy_Improvement_brings_efficiency_to_the_next_level</UrlName>
      <BlogComments />
      <Categories>Challenges and Solutions</Categories>
    </BlogPost>
  </BlogPosts>
</BlogCollection>